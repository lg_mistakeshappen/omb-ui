export type Account = {
	id: string;
	name: string;
	routingNumber: string;
	accountNumber: string;
	openDate: string;
	currentBalance: string;
}
